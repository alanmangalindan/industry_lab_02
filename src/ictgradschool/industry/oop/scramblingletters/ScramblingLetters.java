package ictgradschool.industry.oop.scramblingletters;

import ictgradschool.Keyboard;

public class ScramblingLetters {

    private void start() {

        String wordFromUser = getWordFromUser();

        String newWord = rearrangeWord(wordFromUser);

        char letterFromUser = getLetterFromUser();

        printPosition(newWord, letterFromUser);

        printScrambledWord(newWord);

    }

    // ask user for a word
    private String getWordFromUser() {

        System.out.print("Enter a word: ");
        return Keyboard.readInput();

    }

    private String rearrangeWord(String word) {

        String lettersRemaining = word;
        String newWord = "";

        for (int i = 0; i< word.length(); i++) {
            int randomPosition = (int)(Math.random() * lettersRemaining.length());
            newWord += lettersRemaining.charAt(randomPosition);
            lettersRemaining = lettersRemaining.substring(0, randomPosition) + lettersRemaining.substring(randomPosition + 1);
        }

        return newWord;

    }

    private char getLetterFromUser() {

        System.out.print("Choose a letter: ");
        String fromUser = Keyboard.readInput();
        return fromUser.charAt(0);

    }

    private void printPosition(String newWord, char letter) {

        System.out.println("This letter is now in position " + newWord.indexOf(letter));

    }

    private void printScrambledWord(String newWord) {

        System.out.println("The scrambled word is " + newWord);

    }

    public static void main(String[] args) {

        ScramblingLetters s = new ScramblingLetters();
        s.start();

    }
}
