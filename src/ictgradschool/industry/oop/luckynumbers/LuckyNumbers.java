package ictgradschool.industry.oop.luckynumbers;

import java.util.Random;

/**
 * Write a program which generates 2 random integers between 25 and 30 (inclusive),
 * then uses Math.min() and Math.max() to display them in descending sequence.
 */
public class LuckyNumbers {
    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    private void start() {

        int min = 25;
        int max = 30;

//            Using Random Object
//            Random randomGenerator = new Random();
//            int randomNum1 = randomGenerator.nextInt((max - min) + 1) + min;
//            int randomNum2 = randomGenerator.nextInt((max - min) + 1) + min;

//        for (int i = 0; i < 10; i++) {
            int randomNum1 = (int)(Math.random() * (max - min + 1)) + min;
            int randomNum2 = (int)(Math.random() * (max - min + 1)) + min;

            int first = Math.min(randomNum1, randomNum2);
            int second = Math.max(randomNum1, randomNum2);

//            System.out.println("first: " + first);
//            System.out.println("second: " + second);
            System.out.println("Your lucky numbers are " + first + " and " + second);
//        }

    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        LuckyNumbers ex = new LuckyNumbers();
        ex.start();

    }
}
